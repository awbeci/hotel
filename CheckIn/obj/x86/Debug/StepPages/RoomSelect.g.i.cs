﻿#pragma checksum "..\..\..\..\StepPages\RoomSelect.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "72C1818878EF04F78538EAA9F5007D09"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using CheckIn.UserControls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace CheckIn.StepPages {
    
    
    /// <summary>
    /// RoomSelect
    /// </summary>
    public partial class RoomSelect : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 13 "..\..\..\..\StepPages\RoomSelect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal CheckIn.UserControls.CheckInStep Step;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\..\StepPages\RoomSelect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas canvas1;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\..\StepPages\RoomSelect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label3;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\..\StepPages\RoomSelect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelNum;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\..\StepPages\RoomSelect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label5;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\..\StepPages\RoomSelect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox RoomType;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\..\StepPages\RoomSelect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox RoomNum;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\..\StepPages\RoomSelect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbRoomRate;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\..\StepPages\RoomSelect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dataGrid1;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\..\StepPages\RoomSelect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnUp;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\..\StepPages\RoomSelect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNext;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\..\StepPages\RoomSelect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tbkTotal;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\..\StepPages\RoomSelect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tbkCurrentsize;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\..\StepPages\RoomSelect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelMsg;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\..\StepPages\RoomSelect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Pre;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\..\StepPages\RoomSelect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Next;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\..\StepPages\RoomSelect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Back;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/CheckIn;component/steppages/roomselect.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\StepPages\RoomSelect.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 12 "..\..\..\..\StepPages\RoomSelect.xaml"
            ((System.Windows.Controls.Grid)(target)).Unloaded += new System.Windows.RoutedEventHandler(this.Grid_Unloaded);
            
            #line default
            #line hidden
            
            #line 12 "..\..\..\..\StepPages\RoomSelect.xaml"
            ((System.Windows.Controls.Grid)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Grid_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.Step = ((CheckIn.UserControls.CheckInStep)(target));
            return;
            case 3:
            this.canvas1 = ((System.Windows.Controls.Canvas)(target));
            return;
            case 4:
            this.label3 = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.labelNum = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.label5 = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.RoomType = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.RoomNum = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.TbRoomRate = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.dataGrid1 = ((System.Windows.Controls.DataGrid)(target));
            
            #line 27 "..\..\..\..\StepPages\RoomSelect.xaml"
            this.dataGrid1.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.DataGrid1_OnSelectionChanged);
            
            #line default
            #line hidden
            return;
            case 11:
            this.btnUp = ((System.Windows.Controls.Button)(target));
            
            #line 37 "..\..\..\..\StepPages\RoomSelect.xaml"
            this.btnUp.Click += new System.Windows.RoutedEventHandler(this.btnUp_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.btnNext = ((System.Windows.Controls.Button)(target));
            
            #line 38 "..\..\..\..\StepPages\RoomSelect.xaml"
            this.btnNext.Click += new System.Windows.RoutedEventHandler(this.btnNext_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.tbkTotal = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 14:
            this.tbkCurrentsize = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 15:
            this.labelMsg = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.Pre = ((System.Windows.Controls.Button)(target));
            
            #line 49 "..\..\..\..\StepPages\RoomSelect.xaml"
            this.Pre.Click += new System.Windows.RoutedEventHandler(this.Pre_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.Next = ((System.Windows.Controls.Button)(target));
            
            #line 50 "..\..\..\..\StepPages\RoomSelect.xaml"
            this.Next.Click += new System.Windows.RoutedEventHandler(this.Next_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.Back = ((System.Windows.Controls.Button)(target));
            
            #line 51 "..\..\..\..\StepPages\RoomSelect.xaml"
            this.Back.Click += new System.Windows.RoutedEventHandler(this.Back_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

