﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonLibrary.exception
{
    public class SendRequestException : ApplicationException
    {
        public SendRequestException()
            : base()
        {

        }

        public SendRequestException(string msg)
            : base(msg)
        {

        }

        public SendRequestException(string msg, Exception e)
            : base(msg, e)
        {

        }
    }
}
