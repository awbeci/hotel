﻿using System;
using System.Data;
using HotelCheckIn_BackSystem.DataService.BLL;
using HotelCheckIn_BackSystem.DataService.Model;
using HotelCheckIn_BackSystem.DataService.Dal;

namespace HotelCheckIn_BackSystem.DataService.Bll
{
    public class RoomBll : BaseBll<RoomInfo>
    {
        private readonly RoomDal _roomDal = new RoomDal();
        public override bool Exist(RoomInfo bean)
        {
            throw new NotImplementedException();
        }

        public override void Add(RoomInfo bean)
        {
            throw new NotImplementedException();
        }

        public override void Del(RoomInfo bean)
        {
            throw new NotImplementedException();
        }

        public override void Modify(RoomInfo bean)
        {
            throw new NotImplementedException();
        }

        public override DataTable Query(RoomInfo bean)
        {
            return _roomDal.Query(bean);
        }

        public override DataTable QueryByPage(RoomInfo bean, int page, int rows)
        {
            throw new NotImplementedException();
        }

        public override DataTable QueryByPage(RoomInfo bean, int page, int rows, ref int count)
        {
            throw new NotImplementedException();
        }
    }
}