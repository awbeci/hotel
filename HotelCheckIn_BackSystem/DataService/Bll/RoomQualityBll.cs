﻿using HotelCheckIn_BackSystem.DataService.Dal;
using HotelCheckIn_InterfaceSystem.model;

namespace HotelCheckIn_BackSystem.DataService.Bll
{
    public class RoomQualityBll
    {
        private readonly RoomQualityDal _roomQualityDal =new RoomQualityDal();

        public bool UpdateRoomAndQualityInfo(RoomList roomList, string hotelId)
        {
            return _roomQualityDal.UpdateRoomAndQualityInfo(roomList, hotelId);
        }
    }
}