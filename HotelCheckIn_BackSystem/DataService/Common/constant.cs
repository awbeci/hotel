﻿
namespace HotelCheckIn_BackSystem.DataService.Common
{
    public class constant
    {

        /// <summary>
        /// 无效的终端机
        /// </summary>
        public const int ISDISABLED_N = 1;
        /// <summary>
        /// 有效的终端机
        /// </summary>
        public const int ISDISABLED_Y = 2;
        /// <summary>
        /// 不验证身份证的终端机
        /// </summary>
        public const int ISCHECKID_N = 1;
        /// <summary>
        /// 验证身份证的终端机
        /// </summary>
        public const int ISCHECKID_Y = 2;
        /// <summary>
        /// 默认起始页码:1
        /// </summary>
        public const int PAGE = 1;
        /// <summary>
        /// 默认每页行数:10
        /// </summary>
        public const int ROWS = 10;
        /// <summary>
        /// 最大行数:10000
        /// </summary>
        public const int ROWS_MAX = 10000;
        /// <summary>
        /// 银行卡故障
        /// </summary>
        public const string FAULT_YHK = "101";
        /// <summary>
        /// 发卡机故障
        /// </summary>
        public const string FAULT_FKJ = "102";
        /// <summary>
        /// 身份证识别故障
        /// </summary>
        public const string FAULT_SFZ = "103";
        /// <summary>
        /// 加密键盘故障
        /// </summary>
        public const string FAULT_JMJ = "104";
        /// <summary>
        /// 摄像头故障
        /// </summary>
        public const string FAULT_SXT = "105";
        /// <summary>
        /// 人脸识别故障
        /// </summary>
        public const string FAULT_LSB = "106";
        /// <summary>
        /// PMS通信故障
        /// </summary>
        public const string FAULT_PMS = "107";
        /// <summary>
        /// 其他
        /// </summary>
        public const string FAULT_QT = "108";
        /// <summary>
        /// 校验码验证错误
        /// </summary>
        public const string CHECK_CODE= "check_code_error";
        /// <summary>
        /// 参数验证错误
        /// </summary>
        public const string PARAM_CHECK = "param_check_error";
        /// <summary>
        /// 图片保存失败错误
        /// </summary>
        public const string IMAGE_SAVE = "image_save_error";
        /// <summary>
        /// 订单保存失败错误
        /// </summary>
        public const string ORDER_SAVE = "order_save_error";
        /// <summary>
        /// 其他错误
        /// </summary>
        public const string OTHER = "other_error";
        

    }
}
