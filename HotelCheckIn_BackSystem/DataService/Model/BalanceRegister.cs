﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelCheckIn_BackSystem.DataService.Model
{
    /// <summary>
    /// 登记单结账退房
    /// </summary>
    [Serializable]
    public class BalanceRegister : BaseModel<BalanceRegister>
    {
        /// <summary>
        /// 登记单id
        /// </summary>
        public string RegisterId { get; set; }
        /// <summary>
        /// 结账金额
        /// </summary>
        public string BalanceMoney { get; set; }
        /// <summary>
        /// 付款方式id
        /// </summary>
        public string PaymentTypeId { get; set; }
        /// <summary>
        /// 信用卡类型id
        /// </summary>
        public string CreditCardTypeId { get; set; }
        /// <summary>
        /// 信用卡号
        /// </summary>
        public string CreditCardNo { get; set; }

        public override string ToString()
        {
            return "";
        }
    }
}