﻿using System;
using System.Collections.Generic;
using HotelCheckIn_Interface_Hardware.PMS;

namespace HotelCheckIn_BackSystem.DataService.Model
{
    public class CheckInModel
    {
        /// <summary>
        /// 订单ID
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// 酒店Id
        /// </summary>
        public string HotelId { get; set; }
        /// <summary>
        /// 房间号
        /// </summary>
        public string RoomNum { get; set; }

        /// <summary>
        /// 房间类型
        /// </summary>
        public string RoomType { get; set; }

        /// <summary>
        /// 楼栋
        /// </summary>
        public string Building { get; set; }
        /// <summary>
        /// 预订订单PMS编码
        /// </summary>
        public string OrderPmsCode { get; set; }
        /// <summary>
        /// 入住编码
        /// </summary>
        public string CheckinCode { get; set; }

        /// <summary>
        /// 房价代码
        /// </summary>
        public string RoomCode { get; set; }

        /// <summary>
        /// 总房费
        /// </summary>
        public float RoomRate { get; set; }

        /// <summary>
        /// 成交房价
        /// </summary>
        public float KnockDownPrice { get; set; }

        /// <summary>
        /// 原始房价
        /// </summary>
        public float OriginalPrice { get; set; }

        ///<summary>
        /// 会员卡号
        /// </summary>
        public string VipCardNum { get; set; }

        /// <summary>
        /// 会员级别PMS编码
        /// </summary>
        public string VipCardType { get; set; }

        /// <summary>
        /// 证件类型E11[]
        /// </summary>
        public string[] IdentificationType { get; set; }
        /// <summary>
        /// 民族
        /// </summary>
        public string[] Nation { get; set; }
        /// <summary>
        /// 住址
        /// </summary>
        public string[] Adrress { get; set; }
        /// <summary>
        /// 生日
        /// </summary>
        public DateTime[] Birthday { get; set; }
        /// <summary>
        /// 姓名全拼
        /// </summary>
        public string[] NamePy { get; set; }

        /// <summary>
        /// 入住客人名称
        /// </summary>
        public string[] Name { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string[] Sex { get; set; }

        /// <summary>
        /// 证件号
        /// </summary>
        public string[] IdentityCardNum { get; set; }

        /// <summary>
        /// 入住人数
        /// </summary>
        public int PeopleNum { get; set; }

        /// <summary>
        /// 入住日期
        /// </summary>
        public DateTime CheckInTime { get; set; }

        /// <summary>
        /// 离店日期
        /// </summary>
        public DateTime CheckOutTime { get; set; }

        /// <summary>
        /// 支付类型E01[]
        /// </summary>
        public string[] PayType { get; set; }

        /// <summary>
        /// 支付方式 E02[]
        /// </summary>
        public string[] PayWay { get; set; }

        /// <summary>
        /// 支付金额
        /// </summary>
        public float[] PaymentAmount { get; set; }

        /// <summary>
        /// Hep的支付记录GUID
        /// </summary>
        public string[] HepPayGuid { get; set; }

        /// <summary>
        /// 交易流水号
        /// </summary>
        public string[] Batch { get; set; }

        /// <summary>
        /// 工号PMS编码
        /// </summary>
        public string[] JobNumberPmsCode { get; set; }

        /// <summary>
        /// 操作时间
        /// </summary>
        public DateTime[] OperTime { get; set; }

        /// <summary>
        /// 交易类型E74[]
        /// </summary>
        public string[] TransactionType { get; set; }

        /// <summary>
        /// 卡号
        /// </summary>
        public string[] CardNum { get; set; }

        /// <summary>
        /// 有效期
        /// </summary>
        public string[] Validity { get; set; }

        /// <summary>
        /// 终端ID
        /// </summary>
        public string MacId { get; set; }

        /// <summary>
        /// 图片List数组（图片顺序为：第一个人的身份证图片，第二个人的身份证图片，第一个人的摄像头拍照图片，第二个人的摄像头拍照图片）
        /// </summary>
        public List<byte[]> Images { get; set; }

        /// <summary>
        /// 门锁标识
        /// </summary>
        public int DoorLockSign { get; set; }

        /// <summary>
        /// PMS标识
        /// </summary>
        public int PmsSign { get; set; }

        /// <summary>
        /// 团购商
        /// </summary>
        public string InternetGroup { get; set; }

        /// <summary>
        /// 验证码
        /// </summary>
        public string CheckId { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// 优惠券编号
        /// </summary>
        public string Coupon { get; set; }

        /// <summary>
        /// 发卡（门卡）数
        /// </summary>
        public int CardCount { get; set; }

        /// <summary>
        /// 班别PMS编码
        /// </summary>
        public string ClassPmsCode { get; set; }

        /// <summary>
        /// 关联入住单PMS编码
        /// </summary>
        public string RelevancyCheckinPmsCode { get; set; }

        /// <summary>
        /// 时间属性E12
        /// </summary>
        public string TimeType { get; set; }

        /// <summary>
        /// 散团（1-散客，2-团体）
        /// </summary>
        public string IndividualOrGroup { get; set; }

        /// <summary>
        /// 下单方式E26
        /// </summary>
        public string OrderType { get; set; }

        /// <summary>
        /// 处理时间
        /// </summary>
        public DateTime OrderTime { get; set; }

        /// <summary>
        /// 房号PMS编码
        /// </summary>
        public string RoomNumPmsCode { get; set; }

    }
}