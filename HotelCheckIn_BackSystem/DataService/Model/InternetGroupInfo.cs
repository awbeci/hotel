﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelCheckIn_BackSystem.DataService.Model
{
    /// <summary>
    /// 团购项目表
    /// </summary>
    [Serializable]
    public class InternetGroupInfo : BaseModel<InternetGroupInfo>
    {
        /// <summary>
        /// 前缀码
        /// </summary>
        public string ProjectFrontNum { get; set; }
        /// <summary>
        /// 团购商
        /// </summary>
        public string InternetGroup { get; set; }
        /// <summary>
        /// 团购商id
        /// </summary>
        public string InternetGroupId { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; }
        /// <summary>
        /// 房间类型
        /// </summary>
        public string RoomTypeId { get; set; }
        /// <summary>
        /// 房间类型名称
        /// </summary>
        public string RoomTypeName { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateDt { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string Creater { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateDt { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
        public string Updater { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }
        /// <summary>
        /// 酒店id
        /// </summary>
        public string HotelId { get; set; }
        /// <summary>
        /// 房价
        /// </summary>
        public float Rate { get; set; }
        /// <summary>
        /// 房价代码
        /// </summary>
        public string RateCode { get; set; }

        public override string ToString()
        {
            return "";
        }
    }
    
}