﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelCheckIn_BackSystem.DataService.Model
{
    /// <summary>
    /// 终端机器表
    /// </summary>
    [Serializable]
    public class MachineInfo : BaseModel<MachineInfo>
    {
        /// <summary>
        /// 机器id
        /// </summary>
        public string JqId { get; set; }
        /// <summary>
        /// 故障id
        /// </summary>
        public string FaultId { get; set; }
        /// <summary>
        /// 酒店id
        /// </summary>
        public string HotelId { get; set; }
        /// <summary>
        /// 机器名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 机器ip
        /// </summary>
        public string IP { get; set; }
        /// <summary>
        /// 是否验证身份证（1/是，2/否）
        /// </summary>
        public int IscheckIDcard { get; set; }
        /// <summary>
        /// 是否有效（1/是，2/否）
        /// </summary>
        public int Isdisabled { get; set; }
        /// <summary>
        /// 素材路径
        /// </summary>
        public string MaterialUrl { get; set; }
        /// <summary>
        /// 心跳时间
        /// </summary>
        public DateTime HeartbeatDt { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// 是否下载（1/是，2/否）
        /// </summary>
        public int IsDoland { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Note { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateDt { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string Creater { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateDt { get; set; }
        /// <summary>
        /// 更新人
        /// </summary>
        public string UpdatePerson { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }


        public override string ToString()
        {
            return "";
        }
    }
}
