﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HotelCheckIn_BackSystem.DataService.Model;

namespace HotelCheckIn_BackSystem.DataService.Model.Parameter
{
    public class GetMachine:MachineInfo
    {
        public string Areaid { get; set; }
    }
}