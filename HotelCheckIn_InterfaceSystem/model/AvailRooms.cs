﻿using System.Collections.Generic;

namespace HotelCheckIn_InterfaceSystem.model
{
    public class AvailRooms
    {
        private string request_id;                   //请求ID
        private string total_avail_count;            //可用房间数（包括登记单、预订分房与未分房房间总数）
        private List<Room> avail_room_list;          //可用列表
        private ReturnInfo return_info;              //返回信息

        /// <summary>
        /// 请求ID
        /// </summary>
        public string request_Id
        {
            get { return this.request_id; }
            set { this.request_id = value; }
        }
        /// <summary>
        /// 可用房间数（包括登记单、预订分房与未分房房间总数）
        /// </summary>
        public string total_Avail_Count
        {
            get { return this.total_avail_count; }
            set { this.total_avail_count = value; }
        }
        /// <summary>
        /// 可用列表
        /// </summary>
        public List<Room> avail_Room_List
        {
            get { return this.avail_room_list; }
            set { this.avail_room_list = value; }
        }
        /// <summary>
        /// 返回信息
        /// </summary>
        public ReturnInfo return_Info
        {
            get { return this.return_info; }
            set { this.return_info = value; }
        }
    }
}
