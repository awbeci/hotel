﻿
namespace HotelCheckIn_InterfaceSystem.model
{
    public class DM
    {
        private string id;
        private string name;
        private string code;

        public string Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public string Code
        {
            get { return this.code; }
            set { this.code = value; }
        }
    }
}
