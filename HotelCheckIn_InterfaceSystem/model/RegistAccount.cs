﻿using System.Collections.Generic;

namespace HotelCheckIn_InterfaceSystem.model
{
    public class RegistAccount
    {
        private string request_id;
        private List<Bill> register_account_list;
        private ReturnInfo return_info;

        /// <summary>
        /// 请求ID
        /// </summary>
        public string request_Id
        {
            get { return this.request_id; }
            set { this.request_id = value; }
        }
        /// <summary>
        /// 账单列表
        /// </summary>
        public List<Bill> register_Account_List
        {
            get { return this.register_account_list; }
            set { this.register_account_list = value; }
        }
        /// <summary>
        /// 返回信息
        /// </summary>
        public ReturnInfo return_Info
        {
            get { return this.return_info; }
            set { this.return_info = value; }
        }
    }
}
