﻿using System.Collections.Generic;

namespace HotelCheckIn_InterfaceSystem.model
{
   public class RegistList
    {
        private string request_id;
        private List<Register> regist_list;
        private ReturnInfo return_info;

        /// <summary>
        /// 请求ID
        /// </summary>
        public string request_Id
        {
            get { return this.request_id; }
            set { this.request_id = value; }
        }
        /// <summary>
        /// 登记单列表
        /// </summary>
        public List<Register> regist_List
        {
            get { return this.regist_list; }
            set { this.regist_list = value; }
        }
        /// <summary>
        /// 返回信息
        /// </summary>
        public ReturnInfo return_Info
        {
            get { return this.return_info; }
            set { this.return_info = value; }
        }
    }
}
