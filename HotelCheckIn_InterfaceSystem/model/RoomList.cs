﻿using System.Collections.Generic;

namespace HotelCheckIn_InterfaceSystem.model
{
    public class RoomList
    {
        private string request_id;
        private List<Room> room_list;
        private List<DM> room_types_list;
        private List<DM> room_buildings_list;
        private List<DM> room_floors_list;
        private List<DM> room_directions_list;
        private ReturnInfo return_info;

        /// <summary>
        /// 请求ID
        /// </summary>
        public string request_Id
        {
            get { return this.request_id; }
            set { this.request_id = value; }
        }
        /// <summary>
        /// 房间列表
        /// </summary>
        public List<Room> room_List
        {
            get { return this.room_list; }
            set { this.room_list = value; }
        }
        /// <summary>
        /// 房型信息列表
        /// </summary>
        public List<DM> room_Types_List
        {
            get { return this.room_types_list; }
            set { this.room_types_list = value; }
        }
        /// <summary>
        /// 楼栋列表
        /// </summary>
        public List<DM> room_Buildings_List
        {
            get { return this.room_buildings_list; }
            set { this.room_buildings_list = value; }
        }
        /// <summary>
        /// 放层列表
        /// </summary>
        public List<DM> room_Floors_List
        {
            get { return this.room_floors_list; }
            set { this.room_floors_list = value; }
        }
        /// <summary>
        /// 房屋朝向列表
        /// </summary>
        public List<DM> room_Directions_List
        {
            get { return this.room_directions_list; }
            set { this.room_directions_list = value; }
        }
        /// <summary>
        /// 返回信息
        /// </summary>
        public ReturnInfo return_Info
        {
            get { return this.return_info; }
            set { this.return_info = value; }
        }
    }
}
