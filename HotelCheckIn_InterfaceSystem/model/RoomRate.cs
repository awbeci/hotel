﻿
namespace HotelCheckIn_InterfaceSystem.model
{
    public class RoomRate
    {
        private string room_type_id;            //房型ID
        private string rate_date;               //日期
        private string rate_code;               //价格代码
        private string rate;                    //房价

        /// <summary>
        /// 房型ID
        /// </summary>
        public string room_Type_Id
        {
            get { return this.room_type_id; }
            set { this.room_type_id = value; }
        }
        /// <summary>
        /// 日期
        /// </summary>
        public string rate_Date
        {
            get { return this.rate_date; }
            set { this.rate_date = value; }
        }
        /// <summary>
        /// 价格代码
        /// </summary>
        public string rate_Code
        {
            get { return this.rate_code; }
            set { this.rate_code = value; }
        }
        /// <summary>
        /// 房价
        /// </summary>
        public string Rate
        {
            get { return this.rate; }
            set { this.rate = value; }
        }
    }
}
