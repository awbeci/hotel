﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelCheckIn_Interface_Hardware.BankOfCard
{
    public class RepInfo
    {

        public string RepMsg(string info)
        {
            switch (info)
            {
                case "00": return "交易成功";
                case "01": return "查发卡方";
                case "02": return "查发卡方的特殊条件";
                case "03": return "无效商户";
                case "04": return "没收卡";
                case "05": return "不予承兑";
                case "06": return "出错";
                case "07": return "特殊条件下没收卡";
                case "09": return "请求正在处理中";
                case "12": return "无效交易";
                case "13": return "无效金额";
                case "14": return "无效卡号";
                case "15": return "无此发卡方";
                case "19": return "请重做交易";
                case "20": return "无效应答";
                case "21": return "不作任何处理";
                case "22": return "怀疑操作有误";
                case "23": return "不可接受的交易费";
                case "25": return "查不到原交易记录";
                case "30": return "格式错误";
                case "31": return "交换中心不支持的银行";
                case "33": return "过期的卡（没收卡）";
                case "34": return "有作弊嫌疑（没收卡）";
                case "35": return "与安全保密部门联系（没收卡）";
                case "36": return "受限制的卡（没收卡）";
                case "37": return "呼受理方安全保密部门（没收卡）";
                case "38": return "超过允许的密码次数";
                case "39": return "无此信用卡帐户";
                case "40": return "请求的功能尚不支持";
                case "41": return "挂失卡（没收卡）";
                case "42": return "无此帐户";
                case "43": return "被窃卡（没收卡）";
                case "44": return "无此投资帐户";
                case "45": return "ISO保留使用";
                case "46": return "ISO保留使用";
                case "47": return "ISO保留使用";
                case "48": return "ISO保留使用";
                case "49": return "ISO保留使用";
                case "50": return "ISO保留使用";
                case "51": return "卡内余额不足";
                case "52": return "无此支票帐户";
                case "53": return "无此储蓄卡帐户";
                case "54": return "过期的卡";
                case "55": return "密码错误";
                case "56": return "无此卡记录";
                case "57": return "不允许持卡人进行的交易";
                case "58": return "不支持的交易,请先签到";
                case "59": return "有作弊嫌疑";
                case "60": return "受卡方与安全保密部门联系";
                case "61": return "超出取款金额限制";
                case "62": return "受限制的卡";
                case "63": return "违反安全保密规定";
                case "64": return "原始金额不正确";
                case "65": return "超出取款次数限制";
                case "66": return "受卡方呼受理方安全保密部门";
                case "67": return "捕捉（没收卡）";
                case "68": return "收到的回答太迟";
                case "69": return "ISO保留使用";
                case "70": return "ISO保留使用";
                case "71": return "ISO保留使用";
                case "72": return "ISO保留使用";
                case "73": return "ISO保留使用";
                case "74": return "ISO保留使用";
                case "75": return "密码输入次数超限";
                case "77": return "POS批次与网络中心不一致";
                case "78": return "网络中心需要向POS终端下载数据";
                case "79": return "POS终端上传的脱机数据对帐不平";
                case "89": return "私有保留使用";
                case "90": return "日期切换正在处理";
                case "91": return "发卡方系统故障";
                case "92": return "金融机构无法达到";
                case "93": return "交易违法、不能完成";
                case "94": return "重复交易";
                case "95": return "调节控制错";
                case "96": return "系统故障请重试";
                case "97": return "无此终端";
                case "98": return "交换中心收不到发卡方应答";
                case "99": return "PIN格式错请重新签到";
                case "A0": return "校验错，请重新签到";
                case "A1": return "二磁道长度不符合";
                case "A2": return "三磁道长度不符合";
                case "A3": return "PINKEY校验";
                case "Z0": return "未收到应答";
                case "Z1": return "交易超时，请重试";
                case "Z2": return "因数据包错误引发冲正";
                case "Z3": return "因交易类型错误引发冲正";
                case "Z4": return "因原交易不匹配引发冲正";
                case "Z5": return "因签购单生成失败冲正,请重试";
                case "Y1": return "二磁道信息错误";
                case "Y2": return "冲正或接口转换交易类型错";
                case "Y3": return "init配置文件失败";
                case "Y4": return "未找到原交易";
                case "Y5": return "与密码键盘通讯失败";
                case "Y6": return "报文错误";
                case "Y7": return "冲正文件错误，操作系统异常";
                case "Y8": return "通讯链路异常，请检查连接";
                case "Y9": return "结算文件生成错误";
                case "YA": return "载入动态链接库错误";
                case "X1": return "交易类型错误";
                case "X2": return "接口文件格式错";
                case "X3": return "初始化输出接口文件错";
                case "X4": return "已结算";
                case "X5": return "该交易已经撤消";
                case "X6": return "原交易不是消费交易";
                case "X7": return "输入文件不存在";
                case "ZA": return "校验错，请重新签到";
                case "ZB": return "系统库错误";
                case "W1": return "应收序号不存在";
                case "W2": return "用户有其他方式欠费，不能缴费";
                case "W3": return "供电局数据库繁忙，暂时无法服务";
                case "W4": return "供电局结帐中，暂时无法服务";
                case "W5": return "金额不一致";
                case "W6": return "中心数据库繁忙或无法连接";
                case "W7": return "只允许本人收取的电费(超市收费专用)";
                case "W8": return "其他错误";
                case "W9": return "记账超时";
                case "YY": return "交易超时";
                //case "X7": return "加载键盘dll失败";
                case "X8": return "读取SETTLEDATE_QMF失败";
                case "X9": return "不存在此交易类型";
                case "XA": return "自动结算失败";
                case "XB": return "读取LOGINDATE_QMF失败";
                case "XC": return "读取磁道信息失败";
                case "XD": return "签到失败";
                case "XE": return "键盘写密钥失败";
                case "XF": return "不能识别键盘设备";
                case "XG": return "键盘计算mac错误";
                case "XH": return "解包错";
                case "XI": return "打开串口失败";
                case "XJ": return "读卡器故障";
                //case "YY": return "输入密码超时";
                case "XX": return "用户取消";
                default:
                    return "未知问题";
            }
        }
    }
}
