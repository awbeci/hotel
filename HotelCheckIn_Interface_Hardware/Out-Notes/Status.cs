﻿
namespace HotelCheckIn_Interface_Hardware.Out_Notes
{
    public class Status
    {
        public int ChkSensor1 { get; set; }

        public int ChkSensor2 { get; set; }

        public int DivSensor1 { get; set; }

        public int DivSensor2 { get; set; }

        public int EjtSensor { get; set; }

        public int ExitSensor { get; set; }

        public int NearendSensor { get; set; }



        public int RejectTraysw { get; set; }

        public int CassetteSensor { get; set; }

        public int SolSensor { get; set; }

    }
}
