﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelCheckIn_Interface_Hardware.PMS
{
    public class PmsEnum
    {
         
    }

    /// <summary>
    /// 房间状态
    /// </summary>
    public enum E00
    {
        /// <summary>
        /// 未知
        /// </summary>
        UnKnow,

        /// <summary>
        /// 空净房
        /// </summary>
        EmptyCleanRoom,

        /// <summary>
        /// 空脏房
        /// </summary>
        EmptyDirtyRoom,

        /// <summary>
        /// 在住房
        /// </summary>
        LivingRoom,

        /// <summary>
        /// 维修房
        /// </summary>
        RepairRoom,

        /// <summary>
        /// 临时房
        /// </summary>
        TempRoom,

        /// <summary>
        /// 预分房
        /// </summary>
        PreRoom
    }

    /// <summary>
    /// 房间支付类型
    /// </summary>
    public enum E01
    {
        /// <summary>
        /// 未支付或不限
        /// </summary>
        UnpaidOrNotLimited,

        /// <summary>
        /// 预授权(信用)
        /// </summary>
        PreAuthorization,

        /// <summary>
        /// 预付
        /// </summary>
        Prepaid,

        /// <summary>
        /// 结算
        /// </summary>
        Checkout
    }

    /// <summary>
    /// 支付方式
    /// </summary>
    public enum E02
    {
        /// <summary>
        /// 不限
        /// </summary>
        NotLimited,

        /// <summary>
        /// 现金
        /// </summary>
        Cash,

        /// <summary>
        /// 支票
        /// </summary>
        Check,

        /// <summary>
        /// 汇款
        /// </summary>
        Remittance,

        /// <summary>
        /// 代金券
        /// </summary>
        Vouchers,

        /// <summary>
        /// 会员卡
        /// </summary>
        MembershipCard,

        /// <summary>
        /// 银行卡内卡
        /// </summary>
        BankOfInside,

        /// <summary>
        /// 银行卡外卡
        /// </summary>
        BankOfOutside,

        /// <summary>
        /// 积分
        /// </summary>
        Points,

        /// <summary>
        /// 款待
        /// </summary>
        Entertain,

        /// <summary>
        /// 签单
        /// </summary>
        SignTheBill,

        /// <summary>
        /// 退现金
        /// </summary>
        CashBack,

        /// <summary>
        /// 支付宝
        /// </summary>
        Paypal,

        /// <summary>
        /// 第三方储值卡
        /// </summary>
        ThirdPartyCards
    }

    /// <summary>
    /// 结账状态
    /// </summary>
    public enum E03
    {
        /// <summary>
        /// 不限
        /// </summary>
        NotLimited,

        /// <summary>
        /// 未结账
        /// </summary>
        NotCheckout,

        /// <summary>
        /// 已结账
        /// </summary>
        HasCheckout
    }

    /// <summary>
    /// 账目分类
    /// </summary>
    public enum E04
    {
        /// <summary>
        /// 不限
        /// </summary>
        NotLimited,

        /// <summary>
        /// 消费项
        /// </summary>
        ConsumerItems,

        /// <summary>
        /// 支付项
        /// </summary>
        PaymentItems,

        /// <summary>
        /// 预授权(信用)
        /// </summary>
        PreAuthorization,

        /// <summary>
        /// 预付
        /// </summary>
        Prepaid,

        /// <summary>
        /// 结算
        /// </summary>
        Checkout
    }

    /// <summary>
    /// 证件类型
    /// </summary>
    public enum E11
    {
        /// <summary>
        /// 不限
        /// </summary>
        NotLimited,

        /// <summary>
        /// 身份证
        /// </summary>
        IdCards,

        /// <summary>
        /// 护照
        /// </summary>
        Passpost,

        /// <summary>
        /// 驾照
        /// </summary>
        DriverLicense,

        /// <summary>
        /// 工作证
        /// </summary>
        WorkPermit,

        /// <summary>
        /// 军官证
        /// </summary>
        MilitaryId,

        /// <summary>
        /// 学生证
        /// </summary>
        StudentCard,

        /// <summary>
        /// 台胞证
        /// </summary>
        Mtps,

        /// <summary>
        /// 港澳回乡证
        /// </summary>
        MacauHvps,

        /// <summary>
        /// 其它
        /// </summary>
        Other
    }

    /// <summary>
    /// 时间属性
    /// </summary>
    public enum E12
    {
        /// <summary>
        /// 不限
        /// </summary>
        NotLimited,

        /// <summary>
        /// 过夜房
        /// </summary>
        OvernightRoom,

        /// <summary>
        /// 凌晨房
        /// </summary>
        BeforeDawnRoom,

        /// <summary>
        /// 钟点房
        /// </summary>
        HourRoom,

        /// <summary>
        /// 长包房
        /// </summary>
        LongPackageRoom
    }

    /// <summary>
    /// 会员卡余额操作类型
    /// </summary>
    public enum E22
    {
        /// <summary>
        /// 不限
        /// </summary>
        NotLimited,

        /// <summary>
        /// 储值
        /// </summary>
        StoredValue,

        /// <summary>
        /// 刷卡
        /// </summary>
        Swipe,

        /// <summary>
        /// 转账
        /// </summary>
        Transfer,

        /// <summary>
        /// 赠送
        /// </summary>
        Get,

        /// <summary>
        /// 储值调整
        /// </summary>
        StoredValueAdjustment,

        /// <summary>
        /// 刷卡调整
        /// </summary>
        SwipeAdjustment,

        /// <summary>
        /// 赠送调整
        /// </summary>
        GetAdjustment
    }

    /// <summary>
    /// 下单方式
    /// </summary>
    public enum E26
    {
        /// <summary>
        /// 不限
        /// </summary>
        NotLimited,

        /// <summary>
        /// 入住
        /// </summary>
        WalkInCheckIn,

        /// <summary>
        /// 酒店预订
        /// </summary>
        HotelReservations,

        /// <summary>
        /// CRS预订
        /// </summary>
        CrsReservations,

        /// <summary>
        /// 网站预订
        /// </summary>
        WebReservations,

        /// <summary>
        /// 手机预订
        /// </summary>
        MobileReservations,

        /// <summary>
        /// Hep自助机
        /// </summary>
        HepSelfMachine,


        /// <summary>
        /// 预订平台对接
        /// </summary>
        ReservationsPlatformButt,
    }

    /// <summary>
    /// 价格级别(日期属性)
    /// </summary>
    public enum E29
    {
        /// <summary>
        /// 不限
        /// </summary>
        NotLimited,

        /// <summary>
        /// 平常价
        /// </summary>
        UsualPrice,

        /// <summary>
        /// 周末价
        /// </summary>
        WeekendPrice,

        /// <summary>
        /// 特定价
        /// </summary>
        SpecificPrice
    }

    /// <summary>
    /// 担保类型
    /// </summary>
    public enum E41
    {
        /// <summary>
        /// 不限
        /// </summary>
        NotLimited,

        /// <summary>
        /// 未担保
        /// </summary>
        UnGuaranteed,

        /// <summary>
        /// 担保全部
        /// </summary>
        AllGuarantees,

        /// <summary>
        /// 担保首日
        /// </summary>
        FirstDayGuarantee
    }

    /// <summary>
    /// 担保类型
    /// </summary>
    public enum E43
    {
        /// <summary>
        /// 不限
        /// </summary>
        NotLimited,

        /// <summary>
        /// 未查房
        /// </summary>
        NoRounds,

        /// <summary>
        /// 申请中
        /// </summary>
        Applying,

        /// <summary>
        /// 查房完成
        /// </summary>
        RoundsCompleted,

        /// <summary>
        /// 已撤销
        /// </summary>
        Cancelled
    }

    /// <summary>
    /// 早餐份娄
    /// </summary>
    public enum E51
    {
        /// <summary>
        /// 不限
        /// </summary>
        NotLimited,

        /// <summary>
        /// 单早
        /// </summary>
        SingleBreakfast,

        /// <summary>
        /// 双早
        /// </summary>
        DoubleBreakfase,

        /// <summary>
        /// 全早
        /// </summary>
        FullBreakfast,

        /// <summary>
        /// 无
        /// </summary>
        Nothing
    }

    /// <summary>
    /// 房费类型
    /// </summary>
    public enum E58
    {
        /// <summary>
        /// 不限
        /// </summary>
        NotLimited,

        /// <summary>
        /// 全天房费
        /// </summary>
        FullDayRoomRate,

        /// <summary>
        /// 半天房费
        /// </summary>
        HalfDayRoomRate
    }

    /// <summary>
    /// 房费类型
    /// </summary>
    public enum E65
    {
        /// <summary>
        /// 不限
        /// </summary>
        NotLimited,
                
        /// <summary>
        /// 散客价
        /// </summary>
        IndividualPrice,

        /// <summary>
        /// 网络价
        /// </summary>
        NetPrice,

        /// <summary>
        /// 会员价
        /// </summary>
        MembershipPrice
    }

    /// <summary>
    /// 优惠券类型
    /// </summary>
    public enum E72
    {
        /// <summary>
        /// 不限
        /// </summary>
        NotLimited,

        /// <summary>
        /// 抵扣价
        /// </summary>
        DeductibleCoupons,

        /// <summary>
        /// 特价券
        /// </summary>
        SpecialCoupons,

        /// <summary>
        /// 折扣券
        /// </summary>
        DiscountCoupons
    }

    /// <summary>
    /// 交易类型
    /// </summary>
    public enum E74
    {
        /// <summary>
        /// 不限
        /// </summary>
        NotLimited,

        /// <summary>
        /// 刷卡消费
        /// </summary>
        CreditCardSpending,

        /// <summary>
        /// 消费撤销，退款
        /// </summary>
        ConsumptionCancel,

        /// <summary>
        /// 预授权
        /// </summary>
        PreAuthorication,

        /// <summary>
        /// 预授权取消
        /// </summary>
        PreAuthoricationCancel,

        /// <summary>
        /// 预授权完成
        /// </summary>
        PreAuthoricationCompleted,

        /// <summary>
        /// 预授权完成撤销
        /// </summary>
        PreAuthoricationCompletedCancel,
    }
}
