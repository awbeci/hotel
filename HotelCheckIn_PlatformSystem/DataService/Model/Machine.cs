﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelCheckIn_PlatformSystem.DataService.Model
{
    public class Machine
    {
        /// <summary>
        /// 机器id
        /// </summary>
        public string JqId { get; set; }

        /// <summary>
        /// 故障id
        /// </summary>
        public string FaultId { get; set; }

        /// <summary>
        /// 酒店id
        /// </summary>
        public string HotelId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ip地址
        /// </summary>
        public string IP { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? IscheckIDcard { get; set; }

        /// <summary>
        /// 是否不可用
        /// </summary>
        public int? Isdisabled { get; set; }

        /// <summary>
        /// 素材地址
        /// </summary>
        public string MaterialUrl { get; set; }

        /// <summary>
        /// 心跳时间
        /// </summary>
        public DateTime? HeartbeatDt { get; set; }

        /// <summary>
        /// 机器状态
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateDt { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string Creater { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? UpdateDt { get; set; }

        /// <summary>
        /// 更新人
        /// </summary>
        public string UpdatePerson { get; set; }
        
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
    }
}