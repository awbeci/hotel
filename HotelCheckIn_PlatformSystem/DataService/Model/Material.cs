﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelCheckIn_PlatformSystem.DataService.Model
{
    public class Material
	{
        /// <summary>
        /// 素材id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 素材名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 文件路径
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? DateTime { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public string Operator { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateDt { get; set; }

        /// <summary>
        /// 更新人
        /// </summary>
        public string UpdatePerson { get; set; }
	}
}

