﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HotelCheckIn_PlatformSystem.DataService.Model;

namespace HotelCheckIn_PlatformSystem.DataService.Model.Parameter
{
    public class InitHotelGrid : Hotels
    {
        /// <summary>
        /// 区域名称
        /// </summary>
        public string AreaName { get; set; }
    }
}