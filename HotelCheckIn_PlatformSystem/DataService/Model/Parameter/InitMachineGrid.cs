﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HotelCheckIn_PlatformSystem.DataService.Model;

namespace HotelCheckIn_PlatformSystem.DataService.Model.Parameter
{
    public class InitMachineGrid:Machine
    {
        /// <summary>
        /// 
        /// </summary>
        public string CreateDtPara { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UpdateDtPara { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string HeartbeatDtPara { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string HotelName { get; set; }
    }
}