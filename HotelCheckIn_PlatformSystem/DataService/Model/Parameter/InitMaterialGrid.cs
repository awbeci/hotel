﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HotelCheckIn_PlatformSystem.DataService.Model;

namespace HotelCheckIn_PlatformSystem.DataService.Model.Parameter
{
    public class InitMaterialGrid : Material
    {
        /// <summary>
        /// 
        /// </summary>
        public string DateTimePara { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UpdateDtPara { get; set; }
    }
}